package CAMPS.Admin;

import CAMPS.Common.escapeSpecialChars;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import CAMPS.Connect.DBConnect;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

public class authentication extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession ses = request.getSession(true);
        PrintWriter out = response.getWriter();
        DBConnect db = new DBConnect();
        try {
            request.getServletContext().setAttribute("hostname", (Object) (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/"));
            db.getConnection();
            escapeSpecialChars objesc = new escapeSpecialChars();
            ResourceBundle rb = ResourceBundle.getBundle("CAMPS.Admin.Psalt");
            String uname =  objesc.escapeSpecialChar(request.getParameter("uname")),redirect_url="";
            String ip = request.getHeader("X-FORWARDED-FOR");
            request.getHeader("VIA");
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            ip = ipAddress;
            String signin_status = "";
            String gkey = (String) ses.getAttribute("gkey");
              db.read("SELECT *,DATE_FORMAT(LastLoginTime,\"%d-%m-%Y %H:%i:%s\") AS lastlogin FROM admin.user_master WHERE (upassword=(SELECT SHA1(SHA1(MD5(CONCAT('"+rb.getString("psalt.first")+"',salt.pwdSalt,'" + objesc.escapeSpecialChar(request.getParameter("pword")) + "',salt.pwdSalt,'"+rb.getString("psalt.last")+"')))) AS pwd  FROM admin.user_master salt WHERE username='"+uname+"' AND `status`>0) OR upassword=(SELECT SHA1(SHA1(MD5(CONCAT('"+rb.getString("nsalt.first")+"',salt.pwdSalt,'" + objesc.escapeSpecialChar(request.getParameter("pword")) + "',salt.pwdSalt,'"+rb.getString("nsalt.last")+"')))) AS pwd  FROM admin.user_master salt WHERE username='"+uname+"' AND `status`>0))");
           
            if (db.rs.first()) {
                if (db.rs.getRow() == 1) {
                    signin_status = "1";
                    ses.setAttribute("login", db.rs.getString("userid"));
                    ses.setAttribute("roles", db.rs.getString("roleid"));
                    ses.setAttribute("user_id", db.rs.getString("userid"));
                    ses.setAttribute("lastlogintime", db.rs.getString("lastlogin"));
                    ses.setAttribute("OTPstatus", "0");
                    ses.setAttribute("user_name", db.rs.getString("username"));
                    ses.setAttribute("ss_id", db.rs.getString("userid"));
                    ses.setAttribute("depId", "Test");
                    ses.setAttribute("email", "Test");
                    ses.setAttribute("uType", "staff");
                    db.read1("SELECT startuppage,group_concat(rolename) role_name FROM admin.role_master WHERE id IN (" + db.rs.getString("roleid") + ") ORDER BY startuppage DESC");
                    if (db.rs1.next()) {
                        ses.setAttribute("role_name", db.rs1.getString("role_name"));
                       switch (db.rs.getInt("status")) {
                            case 1:
                                if (db.rs1.getString("startuppage") != null && !db.rs1.getString("StartUpPage").isEmpty()) {
                                    redirect_url=db.rs1.getString("startuppage");
                                } else {
                                    redirect_url="JSP/Welcome/welcomePage.jsp";
                                }
                                break;
                            case 2:
                                redirect_url="JSP/Welcome/change_password.jsp";
                                break;
                            default:
                                redirect_url="JSP/Welcome/welcomePage.jsp";
                        }
                    }
                }
            } else {
                signin_status = "0";
                redirect_url=this.getServletContext().getAttribute("hostname").toString() + "CommonJSP/signin.jsp?er_c=0";
            }
            if (gkey != null) {
                db.insert("INSERT INTO admin.authentication_log VALUES('" + ip + "',NOW(),'" + gkey + "','" + signin_status + "');");
            } else {
                db.insert("INSERT INTO admin.authentication_log VALUES('" + ip + "',NOW(),'" + objesc.escapeSpecialChar(request.getParameter("uname")) + "','" + signin_status + "');");
            }
            response.sendRedirect(redirect_url);
        } catch (SQLException e) {
            out.print(e);
        } catch (Exception e) {
        } finally {
            try {
                out.close();
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(authentication.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }
}
