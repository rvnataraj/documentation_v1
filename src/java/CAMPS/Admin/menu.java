package CAMPS.Admin;


import CAMPS.Connect.DBConnect;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class menu {

    DBConnect db = new DBConnect();
    String menu1 = "";

    public String displaymenu(int parent, String role, String hostname) {
        try {  
            db.getConnection();
            if (parent == 0) {
                menu1 += "<ul class=\"list\">";
            } 
            db.read("SELECT rm.id,rm.label,ifnull(rm.img,'link')img,CASE WHEN rm.link='#' THEN '#' WHEN (rm.extraParameters='1') THEN rm.link ELSE CONCAT('"+hostname+"',rm.link) END link,COUNT(rm1.id) COUNT FROM admin.resource_master rm  INNER JOIN admin.role_resource_mapping rrm ON FIND_IN_SET(rm.id,rrm.menuId) AND rrm.roleId IN ("+role+") AND rm.parentId='"+parent+"' LEFT JOIN (admin.resource_master rm1  INNER JOIN admin.role_resource_mapping rrm1 ON FIND_IN_SET(rm1.id,rrm1.menuId) AND rrm1.roleId IN ("+role+")) ON rm.id=rm1.parentId GROUP BY rm.id ORDER BY rm.sortOrder");
            while (db.rs.next()) {
                if (db.rs.getInt("count") == 0) {
                    menu1 += "<li><a href='" + db.rs.getString("link")+ "' title='" + db.rs.getString("label") + "' ><i class=\"material-icons\">" + db.rs.getString("img") + "</i><span>" + db.rs.getString("label") + "</span> </a></li>";
                } else {     
                     menu a = new menu();
                    menu1 += "<li><a href='" + db.rs.getString("link") + "' class=\"menu-toggle\" title='" + db.rs.getString("label") + "' ><i class=\"material-icons\">" + db.rs.getString("img") + "</i><span>" + db.rs.getString("label") + " </span></a><ul class=\"ml-menu\">" + a.displaymenu(db.rs.getInt("id"), role, hostname) + "</ul></li>";
              }
            }         
        } catch (Exception e) {     
             Logger.getLogger(menu.class.getName()).log(Level.SEVERE, null, e);
        }
        finally{
            try {
                db.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(menu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return menu1;
    }
}
