<%@include file="../../CommonJSP/pageHeader.jsp" %>
<title>UG Student Report </title>
<script>
    $(document).ready(function () {
        var $this = $("#report1");
        var newrows = [];
        $this.find("tr").each(function () {
            var i = 0;
            $(this).find("td").each(function () {
                i++;
                if (newrows[i] === undefined) {
                    newrows[i] = $("<tr></tr>");
                }
                newrows[i].append($(this));
            });
        });
        $this.find("tr").remove();
        $.each(newrows, function () {
            $this.append(this);
        });
    });

</script>
<style> 
    #report1 tr:nth-child(1) {
        text-align: center;
        font-weight: bold;
        border: 0px;
        background: #00000080;
        color: #FFFFFF;
    }
</style>
<% try {

%>
<section class="content">
    <div id='print_area' class="row clearfix">
        <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
            <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                <div class="panel panel-col-light-green">
                    <div class="panel-heading" role="tab" id="headingOne_19">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="true" aria-controls="collapseOne_19">
                                <i class="material-icons">perm_contact_calendar</i> Student Details (UG) </a>
                        </h4>
                    </div>
                    <div id="collapseOne_19" class="panel-collapse collapse in"  role="tabpanel" aria-labelledby="headingOne_19">
                        <div class="panel-body" id='loadstu_det'>
                            <%                                con.getConnection();
                                String head[] = new String[]{"Total Students", "Male Students", "Female Students", "Dayscholar Students", "Hosteller Students", "Counselling Students", "Management Students", "Regular Students", "Lateral Students"};
                                con.read("SELECT scd.year_of_study title,COUNT(*)r1,SUM(IF(spd.gender='male',1,0))r2,SUM(IF(spd.gender='female',1,0))r3,SUM(IF(scd.scholar='dayscholar',1,0))r4,SUM(IF(scd.scholar='Hosteller',1,0))r5,SUM(IF(scd.seat_category='government',1,0))r6,SUM(IF(scd.seat_category='MANAGEMENT',1,0))r7 ,SUM(IF(scd.student_category_id=11,1,0))r8,SUM(IF(scd.student_category_id=12,1,0))r9 FROM camps.student_course_det scd INNER JOIN camps.student_personal_det spd ON spd.enroll_no=scd.enroll_no WHERE scd.student_status IN ('CONTINUING','SUSPENDED') AND scd.degree_level='UG' GROUP BY scd.year_of_study UNION SELECT 'Total' title,COUNT(*)r1,SUM(IF(spd.gender='male',1,0))r2,SUM(IF(spd.gender='female',1,0))r3,SUM(IF(scd.scholar='dayscholar',1,0))r4,SUM(IF(scd.scholar='Hosteller',1,0))r5,SUM(IF(scd.seat_category='government',1,0))r6,SUM(IF(scd.seat_category='MANAGEMENT',1,0))r7 ,SUM(IF(scd.student_category_id=11,1,0))r8,SUM(IF(scd.student_category_id=12,1,0))r9 FROM camps.student_course_det scd INNER JOIN camps.student_personal_det spd ON spd.enroll_no=scd.enroll_no WHERE scd.student_status IN ('CONTINUING','SUSPENDED') AND scd.degree_level='UG'  ORDER BY title");
                                String data = "<table id='report1' border='1' width='100%' class='tbl_new'><td>Details</td>";
                                for (int i = 0; i < head.length; i++) {
                                    data += "<td>" + head[i] + "</td>";
                                }
                                data += "</th>";
                                while (con.rs.next()) {
                                    data += "<tr><td>" + con.rs.getString("title") + "</td>";
                                    for (int i = 0; i < head.length; i++) {
                                        data += "<td>" + con.rs.getString("r" + (i + 1)) + "</td>";
                                    }
                                    data += "</tr>";
                                }
                                data += "</table>";
                                out.print(data);
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<% } catch (Exception e) {
        out.print(e);
    } finally {
        con.closeConnection();
    }
%>
<%@include file="../../CommonJSP/pageFooter.jsp" %>